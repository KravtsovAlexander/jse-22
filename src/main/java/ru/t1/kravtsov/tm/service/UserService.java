package ru.t1.kravtsov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.api.repository.IUserRepository;
import ru.t1.kravtsov.tm.api.service.IPropertyService;
import ru.t1.kravtsov.tm.api.service.IUserService;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.exception.entity.UserNotFoundException;
import ru.t1.kravtsov.tm.exception.field.*;
import ru.t1.kravtsov.tm.model.User;
import ru.t1.kravtsov.tm.util.HashUtil;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IUserRepository repository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IProjectRepository projectRepository,
            @NotNull final IPropertyService propertyService
    ) {
        super(repository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (doesLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return repository.add(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (doesLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (doesEmailExist(email)) throw new EmailExistsException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return repository.add(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (doesLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return repository.add(user);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @NotNull
    @Override
    public Boolean doesLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.doesLoginExist(login);
    }

    @NotNull
    @Override
    public Boolean doesEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.doesEmailExist(email);
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        return remove(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        return remove(user);
    }

    @NotNull
    @Override
    public User remove(@Nullable final User model) {
        if (model == null) throw new UserNotFoundException();
        @Nullable final User user = super.remove(model);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskRepository.deleteAll(userId);
        projectRepository.deleteAll(userId);
        return user;
    }

    @NotNull
    @Override
    public User setPassword(@Nullable String id, @Nullable String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findOneById(id);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @NotNull
    @Override
    public User updateUser(@Nullable String id, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
    }

}
