package ru.t1.kravtsov.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskToProject();

}
