package ru.t1.kravtsov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class ProjectDisplayByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Display project by id.";

    @NotNull
    public static final String NAME = "project-display-by-id";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Project project = getProjectService().findOneById(getUserId(), id);
        displayProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
