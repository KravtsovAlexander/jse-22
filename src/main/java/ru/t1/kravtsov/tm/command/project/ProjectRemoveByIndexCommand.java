package ru.t1.kravtsov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove project by index.";

    @NotNull
    public static final String NAME = "project-remove-by-index";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer input = TerminalUtil.nextNumber();
        @NotNull final Integer index = input - 1;
        @Nullable final Project project = getProjectService().findOneByIndex(index);
        @Nullable final String projectId = project != null ? project.getId() : null;
        getProjectTaskService().removeProjectById(getUserId(), projectId);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
