package ru.t1.kravtsov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Update project by index.";

    @NotNull
    public static final String NAME = "project-update-by-index";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer input = TerminalUtil.nextNumber();
        @NotNull final Integer index = input - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        getProjectService().updateByIndex(getUserId(), index, name, description);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
